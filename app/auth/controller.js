const authModel=require('./model');


exports.getLogin=async(req,res)=>{
    // let message=req.session.message || '';
    // req.session.message=null;

    // let isUserLogin= req.session.isUserLogin || '';
    // req.session.isUserLogin=null;

    res.render('auth/login',{layout:'main' , message , isUserLogin});
};
const messageSession={
    VALID:'شما با موفقیت وارد شدید. در حال هدایت  به ...',
    NOVALID:'کاربری با این مشخصات پیدا نشد'
}

exports.postLogin=async(req,res)=>{
    const {email,password}=req.body;
    const isLogin=await authModel.getUserForLogin(email,password);
    if(!isLogin){
        req.session.message=messageSession.NOVALID;
        req.session.isUserLogin=1;
        return res.redirect('/auth/login');
    }
    req.session.isLogin={
        id:isLogin.id,
        first_name:isLogin.first_name
    }
    // req.session.message=messageSession.VALID;
    res.redirect('/');
            
 
};


exports.getRegister=(req,res)=>{
    res.render('auth/register',{layout:'main'});
};



exports.postRegister=async(req,res)=>{
    const userData=req.body;
    const newUser=await authModel.createUsesr(userData);
    res.redirect('/auth/login');
};    