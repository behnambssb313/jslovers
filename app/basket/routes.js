const express = require('express');
const basketController=require('./controller');
const router=express.Router();



router.get('/',basketController.index);
router.get('/add/:slug',basketController.add);
router.get('/checkout',basketController.checkout);
router.post('/discount/:order_hash',basketController.discount);

module.exports=router; 