const { persianCurrencyToman } = require('../services/currency');

exports.basketProduct = (product) => {
    product.totalPrice = product.count * product.price;
    product.formattedPrice = persianCurrencyToman(product.price);
    product.formattedTotalPrice = persianCurrencyToman(product.totalPrice);
    return product;
}

exports.ordersTotalprice = (total_Price) => {
    const total_Priceed = persianCurrencyToman(total_Price);
    return total_Priceed;
}