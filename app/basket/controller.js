const productmodel=require('../products/model');
const {basketProduct,ordersTotalprice}=require('./presenters');
const orderModel=require('../orders/model');

const crypto=require('crypto');
const basketModel=require('./model');


const paymentModel=require('../payments/model');

exports.index=async(req,res)=>{
    let items=[];
    if(req.session.basket){
        items=Object.keys(req.session.basket).map(id=>{return basketProduct(req.session.basket[id]);});
    }
    res.render('basket/index',{layout:'main',items});
};

exports.add=async(req,res)=>{
    const slug=req.params.slug;
    const product=await productmodel.findBySlug(slug);

    if(!product){
        return res.status(404).render('errors/404', { layout: 'main' });
    }

    if(!('basket' in req.session)){
        req.session.basket = {};
    }

    if (product.id in req.session.basket) {
        let { count } = req.session.basket[product.id];
        req.session.basket[product.id].count = ++count;

    } else {
        req.session.basket[product.id] = {
            id: product.id,
            title: product.title,
            price: product.price,
            count: 1,
            image: product.image
        };
    }

    res.redirect('/basket');
}

exports.checkout=async(req,res)=>{
    const items=req.session.basket;
    console.log(items);
    let totalprice=0;
    let total=0;

    Object.keys(items).forEach(id=>{
        const product=items[id];
        totalprice+=product.price*product.count;
        // total=ordersTotalprice(totalprice);

    })

    const order=await orderModel.create({
        user_id:1,
        total_price:totalprice,
        items_count:Object.keys(items).length,
        shipping_address:'',
        status:0
    });
    res.render('order/details', { layout: 'main', order }); 

}

exports.discount=async(req,res)=>{

    const orderHashing=req.params.order_hash;
    const order=await orderModel.findByHash(orderHashing)
    let total=order.total_price;


    let messageSuccess='';
    let messageLose='';
    const {discountt}=req.body;
    const discountEnd=await paymentModel.findDiscount(discountt);
    if(discountEnd){
        total=total-(total*discountEnd.percent);
        messageSuccess='کد تخفیف شما معتبر است';
    }
    if(!discountEnd){
        console.log('nooooooo');
        messageLose='کد تخفیف شما نا معتبر است';
    }
   

    return res.render('order/details',{layout:'main',order,total,messageSuccess,messageLose});
}