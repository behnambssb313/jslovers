const crypto = require('crypto');
const connection=require('../../database/connection/mysql');
exports.status = {
    PENDING: 0,
    FAILED: 1,
    SUCCESS: 2
};

exports.create=async(params)=>{
    params.hash=crypto.randomBytes(20).toString('hex');
    const db = await connection();
    const [result, fileds] = await db.query(`INSERT INTO payment SET ?`, [params]);
    const [records, fii] = await db.query(`SELECT * FROM payment WHERE id=? LIMIT 1`, [result.insertId]);
    return records[0];
}


exports.update = async (id, params) => {
    const db = await connection();
    const [result, fileds] = await db.query(`UPDATE payment SET ? WHERE id=?`, [params, id]);
    return result.affectedRows > 0;
}

exports.findPaymentByHash=async(hash)=>{
    const db=await connection();
    const[records,fields]=await db.query("SELECT * FROM payment WHERE hash=?",[hash]);
    return records[0];
}



exports.findDiscount=async(codeDiscount)=>{
    const db=await connection();
    const[records,fields]=await db.query("SELECT * FROM discounts WHERE code_discount=?",[codeDiscount]);
    return records[0];
}