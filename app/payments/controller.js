const orederModel=require('../orders/model');
const {doPayment,genereateResNum,verifyPayment}=require('./payment');

const paymentModel=require('./model');
const { getTypeByName } = require('./paymentType');


const {discount}=require('./dis');

exports.startpayment=async(req,res)=>{

    const orderHash=req.params.order_hash;
    const order=await orederModel.findByHash(orderHash);
    const payment_method=req.body.payment_method;

    let total=order.total_price;
    
    const diss=req.body.discount;
    const discount=await paymentModel.findDiscount(diss);
    console.log(discount);
    if(discount){
        console.log('yehhhhhh');
        total=total-(total*0.1);
        console.log(to);
        res.render('order/details',{to})
    }
    if(!discount){
        console.log('nooooooo');
    }

    
    const payment=await paymentModel.create({
        order_id: order.id,
        type: getTypeByName(payment_method),
        amount: total,
        res_num: genereateResNum(),
        status: paymentModel.status.PENDING
    });

    const {success, mustRedirect, redirectUrl, mustRenderView, viewPath, viewParams}=await doPayment(payment,payment_method);

    if (!success) {
        // return res.render('');
    }
    if (mustRedirect) {

        return res.redirect(redirectUrl);
    }
    if (mustRenderView) {
        return res.render(viewPath, { layout: 'main', params: viewParams });
    }
}

exports.verifyPayment = async(req,res)=>{
    const payment_hash=req.params.payment_hash;
    console.log(payment_hash);
    const payment=await paymentModel.findPaymentByHash(payment_hash);
    const {success, ref_num}= await verifyPayment(payment, {
        query:req.query,
        body:req.body
    });    
    if(!success){
        res.status(404).render('gateways/error',{layout:'main'});
    }

    return res.render('gateways/success',{
        layout:'main',params:{
            ref_num
        }
    });

}