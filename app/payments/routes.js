const express=require('express');
const router=express.Router();
const paymentController=require('./controller');

router.post('/start/:order_hash',paymentController.startpayment);
router.post('/verify/:payment_hash', paymentController.verifyPayment);
router.get('/verify/:payment_hash', paymentController.verifyPayment);


module.exports=router;
