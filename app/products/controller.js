const productsModel=require('./model');
const { currencyPrice } = require('./presenter');

exports.index=async(req,res)=>{

    const items=await productsModel.getProducts();
    res.render('products/index',{layout: 'main',items:items.map(product=>currencyPrice(product))});

}