const connection=require('../../database/connection/mysql');


exports.getProducts=async()=>{
    const db=await connection();
    const[records,fields]=await db.query(`SELECT * FROM products`);
    return records;
}


exports.findBySlug=async(slug)=>{
    const db=await connection();
    const[result,fields]=await db.query('SELECT * FROM products WHERE slug=? LIMIT 1',[slug]);
    return result[0];
}