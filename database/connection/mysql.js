const mysql = require('mysql2/promise');

const connection = async () => {

    const handler = await mysql.createConnection({
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    });
    return handler;
};

module.exports = connection;
