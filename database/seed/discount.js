const connection=require('../connection/mysql');
const crypto=require('crypto');

const generateDiscount=async(count=5)=>{
    const db=await connection();
    let discount=[];
    for(let counter=1;counter<=count;counter++){
        const generateDiscount=crypto.randomBytes(2).toString('hex');
        discount.push([
            generateDiscount
        ]);
    };
    console.log(discount)
    db.query("INSERT INTO discounts (`code_discount`) VALUES ?",[discount]);

};
module.exports=generateDiscount;
